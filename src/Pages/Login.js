import React, { useState } from 'react';
import "./css/Sign.css";
import Signup from './Signup';
import Cookies from 'js-cookie';

const Login = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [showSign, setShowSign] = useState(false);
  const [errorMessage, setErrorMessage] = useState('');

  const handleShowSign = () => {
    setShowSign(true);
  };

  function handleLogin(){
    const emailPattern = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/;

    if (!emailPattern.test(email)) {
      setErrorMessage('Please enter a valid email address.');
    } else {

      const loginData = {
        email: email,
        password: password,
      };
  
      fetch('http://localhost:4000/user/logIn', {
        headers: {
          method: 'POST',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(loginData),
      })
        .then((response) => {
          if (response.ok) {
            return response.json();
          } else {
            throw new Error('Email or password incorrect.');
          }
        })
        .then((dataToken) => {
          const token = dataToken.Authorization;
          Cookies.set('AuthToken', token, { expires: 1 });  
          window.location.reload();

          setErrorMessage('');
        })
        .catch((error) => {
          console.error(error);
          setErrorMessage('An error occurred during login.');
        });
      
    }
  }

  return (
    <div>
      {!showSign && (
        <div>
      <h2>Login</h2>
      <form>
        <div>
          <input type="email"  placeholder='Email' value={email} onChange={(e) => setEmail(e.target.value)} />
        </div>
        <div>
          <input type="password" placeholder='Password' value={password} onChange={(e) => setPassword(e.target.value)} />
        </div>
        <button type="button" onClick={handleLogin}>Login</button>
      </form>
      <button className="createAccount" type="button" onClick={handleShowSign}>Create Account</button>
      </div>
      
      )}
      {!showSign && errorMessage && <p>{errorMessage}</p>}

      {showSign && <Signup />}

    </div>
  );
};

export default Login;
