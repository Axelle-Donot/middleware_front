import React, { useState } from 'react';
import './css/FindYourCar.css'

const FindYourCar = () => {
    const [licensePlate, setLicense] = useState('');
    const [carInfo, setCarInfo] = useState(null);

    const handleLicense = () => {
        fetch(`https://vpic.nhtsa.dot.gov/api/vehicles/DecodeVin/${licensePlate}?format=json`)
        .then((response) => response.json())
        .then((data) => {
          console.log(data);
          if(data.Results[8].Value === null) {
            setCarInfo(false);

          }else{
            data = {
                marque: data.Results[7].Value,
                modele: data.Results[9].Value,
                annee: data.Results[10].Value,
                weight : data.Results[28].Value,
                fuel : data.Results[77].Value,
                seatBelt : data.Results[91].Value,
            }; 
            setCarInfo(data);
          }
         
        })
        .catch((error) => {
          console.error('Erreur lors de la requête API:', error);
        });
    };
    return (
        <div>
              <h2>Find Your Car</h2>
                <form>
                    <div>
                        <input className="input" type="text" placeholder='License plate' value={licensePlate} onChange={(e) => setLicense(e.target.value)} />
                    </div>
                    <button type="button" onClick={handleLicense}>Find</button>
                </form>
                {carInfo && (
        <div className='tab'>
          <h3>Your Car :</h3>
          <table>
            <tr>
              <td className='boldText'>Manufacturer</td>
              <td>{carInfo.marque}</td>
            </tr>
            <tr>
                <td className='boldText'>Model</td>
                <td>{carInfo.modele}</td>
              
            </tr>
            <tr>
                <td className='boldText'>Year</td>
                <td>{carInfo.annee}</td>
            </tr>
            <tr>
                <td className='boldText'>Weight</td>
                <td>{carInfo.weight}</td>
            </tr>
            <tr>
                <td className='boldText'>Fuel</td>
                <td>{carInfo.fuel}</td>
            </tr>
            <tr>
                <td className='boldText'>Seat Belt</td>
                <td>{carInfo.seatBelt}</td>
            </tr>
          </table>
        </div>
      )}
      {carInfo === false && (
        <div> No cars found </div>
      )
      }

        </div>
    );
};

export default FindYourCar;