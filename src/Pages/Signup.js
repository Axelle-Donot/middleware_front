import "./css/Sign.css";
import React, { useState } from 'react';
import Login from './Login';
import Cookies from 'js-cookie';

const Signup = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [username, setUsername] = useState('');
  const [showLogin, setLogin] = useState(false);
  const [errorMessage, setErrorMessage] = useState('');

  const handleLogin = () => {
    setLogin(true);
  };

  const handleSignup = () => {
    const emailPattern = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/;
    const passwordPattern = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/;

    if (!emailPattern.test(email)) {
      setErrorMessage('Please enter a valid email address.');
    } else {
      if (!passwordPattern.test(password)) {
        setErrorMessage('The password must contain at least one uppercase letter, one lowercase letter, one number and one special character (@$!%*?&), and must be at least 8 characters long.');
      } else {
        const loginData = {
          username: username,
          email: email,
          password: password,
        };
    
        fetch('http://localhost:4000/user/register', {
          headers: {
            method: 'POST',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(loginData),
        })
          .then((response) => {
            if (response.ok) {
              return response.json();
            } else {
              throw new Error('Email or password incorrect.');
            }
          })
          .then((dataToken) => {
            const token = dataToken.Authorization;
            Cookies.set('AuthToken', token, { expires: 1 });
            window.location.reload();
            setErrorMessage('');
          })
          .catch((error) => {
            console.error(error);
            setErrorMessage('An error occurred during login.');
          });
        // Le mot de passe est valide, vous pouvez procéder à la logique de connexion ici.
        setErrorMessage('');
        // Ajoutez le reste de votre logique de connexion ici
      }
    }
  };

  return (
    <div>
      {!showLogin && (
        <div>

      <h2>Sign up</h2>
      <form>
        <div>
          <input type="text" placeholder='Username' value={username} onChange={(e) => setUsername(e.target.value)} />
        </div>
        <div>
          <input type="email" placeholder='Email' value={email} onChange={(e) => setEmail(e.target.value)} />
        </div>
        <div>
          <input type="password" placeholder='Password *' value={password} onChange={(e) => setPassword(e.target.value)} />
        </div>
        <button type="button" onClick={handleSignup}>Sign up</button>
      </form>
      <button  className="createAccount" type="button" onClick={handleLogin}>Login</button>
      <p>*Password must contain uppercase, lowercase, number and special character</p>

      </div>
      )}
      {!showLogin && errorMessage && <p>{errorMessage}</p>}

      {showLogin && <Login />}


    </div>
  );
};

export default Signup;
