import React, { useState, useEffect } from 'react';
import Cookies from 'js-cookie';

const Account = () => {
  const [username, setUsername] = useState('');
  const [email, setEmail] = useState('');

  const handleLogout = () => {
    const token = Cookies.get('AuthToken');

      fetch('http://localhost:4000/user/logOut', {
        headers: {
          method: 'POST',
          Authorization: `${token}`,
        },
      })
        .then((response) => {
          if (!response.ok) {
            throw new Error('Unauthorized');
          }
          Cookies.remove('AuthToken');
          window.location.reload();
        })
        .catch((error) => {
          console.error(error);
          handleLogout(); 
        });
    
  };

  useEffect(() => {
    const token = Cookies.get('AuthToken');

    if (token) {
      fetch('http://localhost:4000/user', {
        headers: {
          Authorization: `${token}`,
        },
      })
        .then((response) => {
          if (!response.ok) {
            throw new Error('Unauthorized');
          }
          return response.json();
        })
        .then((data) => {
          setUsername(data.username);
          setEmail(data.email);
        })
        .catch((error) => {
          console.error(error);
          handleLogout(); 
        });
    } else {
      handleLogout(); 
    }
  }, []);

  return (
    <div>
      <h2>Account</h2>
      <p>Username: {username}</p>
      <p>Email: {email}</p>
      <button type="button" onClick={handleLogout}>
        Log out
      </button>
    </div>
  );
};

export default Account;
