import React from "react";
import useSWR from "swr";
import { styled } from "@mui/material/styles";
import Card from "@mui/material/Card";
import CardHeader from "@mui/material/CardHeader";
import CardMedia from "@mui/material/CardMedia";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";

const Products = () => {
  const fetcher = (url) => fetch(url).then((r) => r.json());

  const { data, error } = useSWR("http://localhost:4000/products", fetcher);

  console.table(data);

  return (
    <div>
      Catalog
      {data
        ? data.map((elem) => {
            <Card sx={{ maxWidth: 300 }}>
              <CardHeader title={data.name} />
              <CardMedia
                component="img"
                height="100"
                image="/voitureLogo.jpg"
                alt="Paella dish"
              />
              <CardContent>
                <Typography variant="body2">
                  La sécurité et le confort d'une voiture sont essentiels pour
                  les trajets en famille, offrant une protection et un espace
                  confortable lors des déplacements.
                </Typography>
                <Typography variant="body1">{data.price}</Typography>
              </CardContent>
            </Card>;
          })
        : []}
    </div>
  );
};

export default Products;
