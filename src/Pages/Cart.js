import React, { useState, useEffect } from 'react';
import Cookies from 'js-cookie';
import './css/Cart.css';

const Cart = () => {
  const [panier, setPanier] = useState([]);

  function removeAll() {
    const token = Cookies.get('AuthToken');

    if (token) {
      fetch('http://localhost:4000/remove/all', {
        headers: {
          method : 'POST',
          Authorization: `${token}`,
        },
      })
        .then((response) => {
          if (!response.ok) {
            throw new Error('Unauthorized');
          }
          return response.json();
        })
        .then((data) => {
          setPanier(data);
        })
        .catch((error) => {
          console.error(error);
          // Gérer les erreurs d'authentification ou de requête ici
        });
    } else {
      alert('Authentication failed')
      // Gérer le cas où le token n'est pas trouvé dans les cookies
    }
  }

  useEffect(() => {
    const token = Cookies.get('AuthToken');

    if (token) {
      fetch('http://localhost:4000/cart', {
        headers: {
          Authorization: `${token}`,
        },
      })
        .then((response) => {
          if (!response.ok) {
            throw new Error('Unauthorized');
          }
          return response.json();
        })
        .then((data) => {
          setPanier(data);
        })
        .catch((error) => {
          console.error(error);
          // Gérer les erreurs d'authentification ou de requête ici
        });
    } else {
      alert('Authentication failed')
      // Gérer le cas où le token n'est pas trouvé dans les cookies
    }
  }, []);

  function pay(){
    const token = Cookies.get('AuthToken');

    if (token) {
      fetch('http://localhost:4000/cart/pay', {
        headers: {
          method : 'POST',
          Authorization: `${token}`,
        },
      })
        .then((response) => {
          if (!response.ok) {
            throw new Error('Unauthorized');
          }
          return response.json();
        })
        .then((data) => {
          if (data === "Success"){
            alert("Payment success")
            window.location.reload();
          }
        })
        .catch((error) => {
          console.error(error);
          // Gérer les erreurs d'authentification ou de requête ici
        });
    } else {
      alert('Authentication failed')
      // Gérer le cas où le token n'est pas trouvé dans les cookies
    }
  }

  function removeProduct(id){
    const token = Cookies.get('AuthToken');
    const productId = {
      productId: id,
    };
    if (token) {
      fetch('http://localhost:4000/remove', {
        headers: {
          method : 'POST',
          Authorization: `${token}`,
        },
        body: JSON.stringify(productId),
      })
        .then((response) => {
          if (!response.ok) {
            throw new Error('Unauthorized');
          }
          return response.json();
        })
        .then((data) => {
          setPanier(data);
        })
        .catch((error) => {
          console.error(error);
        });
    } else {
      alert('Authentication failed')
    }
  }


  const subtotal = panier.reduce((acc, produit) => acc + produit.price * produit.quantity, 0);

  return (
    <div className='cart'>
      <h2>Client Cart</h2>
      <div className='center-table'>
        <table>
          <thead>
            <tr>
              <th>Name</th>
              <th>Price</th>
              <th>Quantity</th>
              <th>Delete</th>
            </tr>
          </thead>
          <tbody>
            {panier.map((produit) => (
              <tr key={produit.id}>
                <td>{produit.name}</td>
                <td>{produit.price}</td>
                <td>{produit.quantity}</td>
                <td onClick={() => removeProduct(produit.id)}>Delete the product</td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>

      <div className='total'>
        <button type='button' onClick={removeAll}>Remove All</button>
        <table>
          <tbody>
            <tr>
              <td>Subtotal : {subtotal}</td>
            </tr>
          </tbody>
        </table>
        <button onClick={pay}>Pay</button>
      </div>
    </div>
  );
};

export default Cart;
