import "./css/LandingPage.css";
import React from "react";
import { Tabs, Tab } from "@mui/material";
import Account from "./Account";
import Cart from "./Cart";
import Signup from "./Signup";
import Login from "./Login";
import Products from "./Products";
import FindYourCar from "./FindYourCar";

const LandingPage = () => {
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  function checkCookieExistence(cookieName) {
    const cookies = document.cookie.split(';'); // Divise la chaîne de cookies en un tableau
    for (let i = 0; i < cookies.length; i++) {
      const cookie = cookies[i].trim();
      if (cookie.startsWith(`${cookieName}=`)) {
        return true; 
      }
    }
    return false; 
  }


  return (
    <div>
      <div className="flexHeader">
          {!checkCookieExistence("AuthToken") && ( <Tabs className="tabs" value={value} onChange={handleChange} centered>
            <Tab value={0} className="tab" label="Catalog" />
            <Tab value={1} className="tab" label="Find Your Car" />
            <Tab value={3} className="tab" label="Login/Register" />          
          </Tabs>)}
          {checkCookieExistence("AuthToken") && ( <Tabs className="tabs" value={value} onChange={handleChange} centered>
            <Tab value={0} className="tab" label="Catalog" />
            <Tab value={1} className="tab" label="Find Your Car" />
            <Tab value={5} className="tab" label="Cart" />
            <Tab value={2} className="tab" label="Account" />
          </Tabs>)}        
      
      </div>
      <div>
        {value === 0 ? <Products /> : undefined}
        {value === 1 ? <FindYourCar /> : undefined}
        {value === 2 ? <Account /> : undefined}
        {value === 3 ? <Login /> : undefined}
        {value === 4 ? <Signup /> : undefined}
        {value === 5 ? <Cart /> : undefined}
      </div>

    </div>
    
  );
};

export default LandingPage;
